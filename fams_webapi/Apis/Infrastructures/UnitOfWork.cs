﻿using Application;
using Application.Repositories;
using Infrastructures.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        //private readonly IChemicalRepository _chemicalRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITrainingRepository _trainingRepository;
        private readonly ILessonRepository _lessonRepository;
        private readonly IProgramDetailRepository _programDetailRepository;
        private readonly IClassRepository _classRepository;
        private readonly ITokenRepository _tokenRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly ISyllabusRepository _syllabusRepository;
        private readonly IUnitRepository _unitRepository;       
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IAttendanceRepository _attendanceRepository;
        public UnitOfWork(AppDbContext dbContext,
            //IChemicalRepository chemicalRepository,
            IUserRepository userRepository,
            ITrainingRepository trainingRepository,
            ILessonRepository lessonRepository,
            IProgramDetailRepository programDetailRepository,
            IClassRepository classRepository,
            ITokenRepository tokenRepository,
            IRoleRepository roleRepository,
            ISyllabusRepository syllabusRepository,
            IUnitRepository unitRepository,
            IFeedbackRepository feedbackRepository,
            IAttendanceRepository attendanceRepository
            )          

        {
            _dbContext = dbContext;
            //_chemicalRepository = chemicalRepository;
            _userRepository = userRepository;

            _trainingRepository = trainingRepository;
            _lessonRepository = lessonRepository;
            _programDetailRepository = programDetailRepository;
            _classRepository = classRepository;
            _tokenRepository = tokenRepository;
            _roleRepository = roleRepository;
            _syllabusRepository = syllabusRepository;
            _unitRepository = unitRepository;
            _feedbackRepository = feedbackRepository;
            _attendanceRepository = attendanceRepository;
        }
        //public IChemicalRepository ChemicalRepository => _chemicalRepository;
        public IUserRepository UserRepository => _userRepository;
        public ISyllabusRepository SyllabusRepository => _syllabusRepository;
        public ITrainingRepository TrainingRepository => _trainingRepository;
        public IProgramDetailRepository ProgramDetailRepository => _programDetailRepository;
        public ILessonRepository LessonRepository => _lessonRepository;
        public IClassRepository ClassRepository => _classRepository;
        public IRoleRepository RoleRepository => _roleRepository;
        public ITokenRepository TokenRepository => _tokenRepository;
        public IUnitRepository UnitRepository => _unitRepository;
        public IFeedbackRepository FeedbackRepository => _feedbackRepository;
        public IAttendanceRepository AttendanceRepository => _attendanceRepository;

        public async Task<int> SaveChangeAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
