﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Application.Utils;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<bool> CheckEmailExited(string email) => _dbContext.Users.AnyAsync(u => u.Email == email);

        public async Task<User> GetUserByEmailAndPasswordHash(string email, string passwordHash)
        {
            bool isPassWordCorrect = false;
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(record => record.Email == email);
            if (user is null)
            {
                throw new KeyNotFoundException("Email and password is not correct.");
            }
            isPassWordCorrect = StringUtils.Verify(passwordHash, user.Password);   
            if (isPassWordCorrect == false)
            {
                throw new KeyNotFoundException("Password is not correct.");
            }
            return user;
        }

        public async Task<User> GetUserByEmail(string email)
        {

            var user = await _dbContext.Users
                .FirstOrDefaultAsync(record => record.Email == email);

            if (user is null)
            {
                throw new KeyNotFoundException("User not found.");
            }


            return user;
        }

        public async Task<User> FindUserById(int id)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.UserId == id);
            if (user is null)
            {
                throw new KeyNotFoundException("User does not exists");
            }

            return user;
        }




        //public async Task<User> FindUserByFullName(string FullName)
        //{
        //    var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.FullName.Contains(FullName));
        //    if (user is null)
        //    {
        //        throw new KeyNotFoundException("User does not exists");
        //    }

        //    return user;
        //}

        //public async Task<User> FindUserByEmail(string Email)
        //{
        //    var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Email.Contains(Email));
        //    if (user is null)
        //    {
        //        throw new KeyNotFoundException("User does not exists");
        //    }

        //    return user;
        //}
    }
}
