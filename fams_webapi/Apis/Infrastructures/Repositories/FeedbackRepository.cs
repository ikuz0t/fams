﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class FeedbackRepository : GenericRepository<Feedback>, IFeedbackRepository
    {
        private readonly AppDbContext _dbContext;

        public FeedbackRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }
        public async Task<Feedback> GetFeedbackByIdAsync(int id) => await _dbContext.Feedbacks.FindAsync(id);
        public async Task<IEnumerable<Feedback>> GetNonDeletedFeedbackAsync() =>
            await _dbContext.Feedbacks.Where(f => f.IsDeleted != true).ToListAsync();
        public Task<Feedback> returnNULL() => null;
    }
}
