﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.TrainingViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TrainingRepository : GenericRepository<Training>, ITrainingRepository
    {
        private readonly AppDbContext _dbContext;
        public TrainingRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }
        public Task<bool> CheckTrainingNameExisted(string trainingName) => _dbContext.Trainings.AnyAsync(c => c.TrainingName == trainingName);

        public Task<Training?> CheckTrainingIdExisted(int programID) => _dbContext.Trainings.FirstOrDefaultAsync(t => t.ProgramID == programID);
        public async Task<Training> GetTrainingById(int id)
        {
            var trainingProgram = await _dbContext.Trainings
                .FirstOrDefaultAsync(record => record.ProgramID == id);
            if (trainingProgram is null)
            {
                throw new Exception("Training program with id: " + id + " doesn't exist");
            }
            return trainingProgram;
        }
        public async Task<ViewProgramDetailsDTO> GetProgramDetails(int id)
        {
            var programDetails = await _dbContext.Trainings
                .Where(record => record.ProgramID == id)
                .Select(record => new ViewProgramDetailsDTO()
                {
                    TrainingName = record.TrainingName,
                    TrainingDate = record.TrainingDate,
                    CreatedBy = record.CreatedBy,
                    CreatedDate = record.CreatedDate,
                    Duration = record.Duration,
                    Status = record.Status,
                    Syllabus = record.ProgramDetails
                    .OrderBy(record => record.Sequence)
                    .Select(record => record.Sysllabus)
                    .ToList(),
                }).FirstOrDefaultAsync();

                if (programDetails is null)
                {
                throw new Exception("Training program with id: " + id + " doesn't exist");
                }
            return programDetails;
        }

        public async Task<List<Training>> GetTrainingByName(string searchValue)
        {
            var listSearch = await _dbContext.Trainings
                .Where(record => record.TrainingName.ToLower().Contains(searchValue.ToLower()))
                .ToListAsync();
            if (listSearch.Count is 0)
            {
                throw new Exception("Training program doesn't exist");
            }
            return listSearch;
        }


    }
}
