﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AttendanceViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class AttendanceRepository : GenericRepository<Attendance>, IAttendanceRepository
    {
        private readonly AppDbContext _dbContext;
        public AttendanceRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }
        public async Task<Attendance> GetAttendanceByIdAsync(int id) => await _dbContext.Attendances.FindAsync(id);
        public async Task<IEnumerable<Attendance>> GetAttendanceListAsync() =>
            await _dbContext.Attendances.Where(f => f.IsDeleted != true).ToListAsync();

        public async Task<List<AttendanceListDTO>> SearchAttendanceByUnitNameAsync(string keyword)
        {
            var units = await _dbContext.Units
                .Where(u => u.UnitName.Contains(keyword))
                .ToListAsync();

            var unitIds = units.Select(u => u.UnitID);

            var attendances = await _dbContext.Attendances
                .Where(a => unitIds.Contains(a.UnitID))
                .ToListAsync();

            var attendanceList = attendances.Select(item => new AttendanceListDTO
            {
                ClassID = item.ClassID,
                UnitID = item.UnitID,
                Status = item.Status,
                Date = item.Date,
                UserID = item.UserID
            }).ToList();

            return attendanceList;
        }
    }
}
