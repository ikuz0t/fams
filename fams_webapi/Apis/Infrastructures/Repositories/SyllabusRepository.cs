﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.SyllabusViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Infrastructures.Repositories
{
    public class SyllabusRepository : GenericRepository<Syllabus>, ISyllabusRepository
    {
        private readonly AppDbContext _dbContext;
        public SyllabusRepository(AppDbContext dbContext,
           ICurrentTime timeService,
           IClaimsService claimsService)
           : base(dbContext,
        timeService,
                 claimsService)
        {
            _dbContext = dbContext;
        }
        public bool CheckExistsSyllabus(int id)
        {
            return _dbContext.Syllabuses.Any(x => x.SyllabusID == id);
        }
        public bool Save()
        {
            var saved = _dbContext.SaveChanges();
            return saved > 0 ? true : false;
        }
        public async Task<SyllabusDetailDTO> GetSyllabusDetail(int id)
        {
            var syllabus = await _dbContext.Syllabuses
              .Include(x => x.Units)
              .ThenInclude(u => u.Lessons)
              .FirstOrDefaultAsync(x => x.SyllabusID == id && (x.IsDeleted == false || x.IsDeleted == null));
            if (syllabus == null || !CheckExistsSyllabus(syllabus.SyllabusID))
            {
                return null;
            }
            var detail = _dbContext.Syllabuses.Where(x => x.SyllabusID == id)
                .Select(x => new SyllabusDetailDTO
                {
                    SyllabusID = x.SyllabusID,
                    SyllabusName = x.SyllabusName,
                    SyllabusAudience = x.SyllabusAudience,
                    SyllabusStatus = x.SyllabusStatus,
                    SyllabusCode = x.SyllabusCode,
                    SyllabusDate = x.SyllabusDate,
                    SyllabusBy = x.SyllabusBy,
                    Duration = x.Duration,
                    Description = x.Description,
                    SyllabusAssessment = x.SyllabusAssessment,
                    Units = x.Units.Where(u => u.IsDeleted == false || u.IsDeleted == null)
                    .Select(o => new UnitListDTO
                    {
                        UnitID = o.UnitID,
                        UnitName = o.UnitName,
                        Description = o.Description,
                        NumberLesson = o.NumberLesson,
                        SyllabusID = o.SyllabusID,
                        Lessons = o.Lessons.Where(l => l.IsDeleted == false || l.IsDeleted == null)
                        .Select(p => new LessonListDTO
                        {
                            ContentID = p.ContentID,
                            Duration = p.Duration,
                            Content = p.Content,
                            Materials = p.Materials,
                            TrainingFormat = p.TrainingFormat,
                            UnitID = p.UnitID,
                        }).ToList()
                    }).ToList()
                }).FirstOrDefault();
            return detail;
        }
        public List<Syllabus> GetSyllabusList()
        {
            var syllabuses = _dbContext.Syllabuses
            .Where(x => x.IsDeleted == false ||
                        x.IsDeleted == null).ToList();
            return syllabuses;
        }

        public List<Syllabus> GetSyllabusListBy(string input)
        {
            var aa = _dbContext.Syllabuses.Where(x => x.SyllabusCode.Contains(input) ||
                                                    x.SyllabusName.Contains(input) ||
                                                    x.SyllabusBy.Contains(input)).ToList();
            if (aa == null)
            {
                return null;
            }
            return aa;
        }
        public async Task DeleteSyllaaa(Syllabus syllabus)
        {
            _dbContext.Syllabuses.Remove(syllabus);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<Syllabus> GetIdByAsync(int id)
        {

            var aaa = await _dbContext.Syllabuses.FirstOrDefaultAsync(record => record.SyllabusID == id);
            if (aaa is null)
            {
                return null;
            }
            return aaa;
        }


        //public async Task<Syllabus> CheckIsDelete(bool isRemove)
        //{
        //    if (isRemove)
        //    {
        //        return null;
        //    }
        //    var result = await _dbContext.Syllabuses.Where(r => r.IsDeleted == false).ToList();
        //    return result;
        //}



        public async Task<Syllabus> GetSyllabusByID(int syllabusID)
        {
            var Syllabus = await _dbContext.Syllabuses.FirstOrDefaultAsync(s => s.SyllabusID == syllabusID);
            if (Syllabus is null)
            {
                throw new Exception("Syllabus does not exists");
            }
            return Syllabus;
        }

        public Task<bool> CheckCodeExited(string code) => _dbContext.Syllabuses.AnyAsync(s => s.SyllabusCode == code);

        public async Task AddUnitAsync(Unit unit)
        {
            await _dbContext.Units.AddAsync(unit);
            //await _dbContext.SaveChangesAsync();
        }
        public async Task AddLessonAsync(Lesson lesson)
        {
            await _dbContext.Lessons.AddAsync(lesson);
            //await _dbContext.SaveChangesAsync();

        }
    }
}
