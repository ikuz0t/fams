﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Domain.Entities
{
	//[Table("TblRole")]
	public class Role : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int RoleID { get; set; }

		[StringLength(255)]
		public string RoleName { get; set; }

		//public ICollection<Permission> Permissions { get; set; }
		public ICollection<User> Users { get; set; }

	}
}
