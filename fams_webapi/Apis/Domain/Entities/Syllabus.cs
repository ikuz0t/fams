﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Syllabus : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SyllabusID { get; set; }

		[StringLength(100)]
		public string SyllabusName { get; set; }

		[StringLength(50)]
		public string SyllabusCode { get; set; }

		public DateTime SyllabusDate { get; set; }

		[StringLength(100)]
		public string SyllabusBy { get; set; }

		[StringLength(50)]
		public string Duration { get; set; }

		[StringLength(255)]
		public string Description { get; set; }

		public int? SyllabusOutline { get; set; }

		public int? SyllabusAudience { get; set; }

		[StringLength(20)]
		public string SyllabusStatus { get; set; }

		[StringLength(255)]
		public string SyllabusAssessment { get; set; }
		
		public ICollection<ProgramDetails> ProgramDetails { get; set; }
		public ICollection<Unit> Units { get; set; }
		
	}

}
