﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class ClassStudents
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ClassStudentsId { get; set; }

		public int ClassId { get; set; }

		public int UserId { get; set; }

		[ForeignKey("ClassId")]
		public Class Class { get; set; }

		[ForeignKey("UserId")]
		public User User { get; set; }

	}
}
