﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Domain.Entities
{
	//[Table("TblUser")]
	public class User : BaseEntity
    {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int UserId { get; set; }

		public int RoleId { get; set; }

		[Required]
		[StringLength(150)]
		public string Email { get; set; }

		[Required]
		[StringLength(255)]
		public string Password { get; set; }

		[StringLength(100)]
		public string FullName { get; set; }

		public DateTime DateOfBirth { get; set; }

		[StringLength(10)]
		public string Gender { get; set; }

		public int Level { get; set; }

		[StringLength(20)]
		public string Status { get; set; }

		[MaxLength]
		public string? RefreshToken { get; set; }

		public DateTime? RefreshTokenExpiryDate { get; set; }

		[ForeignKey("RoleId")]
		public Role Role { get; set; }

		public ICollection<Attendance> Attendances { get; set; }
		public ICollection<ClassStudents> ClassStudents { get; set; }
	}
}
