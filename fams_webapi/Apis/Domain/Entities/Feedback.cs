﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Domain.Entities
{
    public class Feedback : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FeedbackId { get; set; }
        public int StudentId { get; set; }
        public int TrainerId { get; set; }
        public int FeedbackStatus { get; set; }
        public string? FeedbackMessage { get; set; }


        [ForeignKey("StudentId")]
        public User Student { get; set; }
        [NotMapped]
        public User Trainer { get; set; }
    }
}
