﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Lesson : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ContentID { get; set; }

		[StringLength(50)]
		public string Duration { get; set; }

		public string Content { get; set; }

		[StringLength(int.MaxValue)]
		public string Materials { get; set; }

		[StringLength(100)]
		public string TrainingFormat { get; set; }

		[ForeignKey("UnitID")]
		public int UnitID { get; set; }

		public Unit Unit { get; set; }
	}
}
