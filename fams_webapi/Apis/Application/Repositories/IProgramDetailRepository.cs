﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IProgramDetailRepository : IGenericRepository<ProgramDetails>
    {
        Task<ProgramDetails> GetProgDById(int programDetailId);
    }
}
