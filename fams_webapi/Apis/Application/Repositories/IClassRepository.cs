﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IClassRepository : IGenericRepository<Class>
	{
		Task<Class> GetClassByIdAsync(int classId);

		//Task<List<Class>> GetAllClass();

		//Task<ClassDetails> GetClassDetailAsync(int classId);

		Task<Training> GetTrainingByIdAsync(int programId);

		Task<List<Syllabus>> GetSyllabusByTrainingIdAsync(int programId);

		Task<List<Class>> GetClassByKeyNameOrCodeAsync(string keyword);

		Task<bool> CheckClassNameExistedAsync(string className);

		Task<bool> CheckClassCodeExistedAsync(string code);

		Task<Class?> CheckClassExistedAsync(int classId);

		Task AddClassStudentsAsync(ClassStudents classStudents);

		Task<bool> CheckStudentExistedInClassAsync(int classId, int userId);

		Task<List<ClassStudents>> GetStudentInClassAsync(int classId);
	}
}
