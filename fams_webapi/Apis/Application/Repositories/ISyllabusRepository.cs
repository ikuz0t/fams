﻿using Application.ViewModels.SyllabusViewModels;
using Domain.Entities;


using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Application.Repositories
{
    public interface ISyllabusRepository : IGenericRepository<Syllabus>
    {   
        List<Syllabus> GetSyllabusList();
        List<Syllabus> GetSyllabusListBy(string input);
        Task<SyllabusDetailDTO> GetSyllabusDetail(int id);        
        Task DeleteSyllaaa(Syllabus syllabus);
        Task<Syllabus> GetIdByAsync(int id);       
        bool Save();
        bool CheckExistsSyllabus(int id);
        //Task UpdateSyllabus(SyllabusUpdateDTO syllabus);   
        Task<Syllabus> GetSyllabusByID(int syllabusID);
        //bool Save();     
        public Task<bool> CheckCodeExited(string code);
    }
}
