﻿using Application.ViewModels.TrainingViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITrainingRepository : IGenericRepository<Training>
    {
        Task<List<Training>> GetTrainingByName(string searchValue);
        Task<Training> GetTrainingById(int id);
        Task<ViewProgramDetailsDTO> GetProgramDetails(int id);
        Task<bool> CheckTrainingNameExisted(string trainingName);       
        Task<Training?> CheckTrainingIdExisted(int programId);
    }
}
