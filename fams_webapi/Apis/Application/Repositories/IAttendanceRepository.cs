﻿using Application.ViewModels.AttendanceViewModels;
using Domain.Entities;


namespace Application.Repositories
{
    public interface IAttendanceRepository : IGenericRepository<Attendance>
    {
        public Task<Attendance> GetAttendanceByIdAsync(int id);
        public Task<IEnumerable<Attendance>> GetAttendanceListAsync();

        public Task<List<AttendanceListDTO>> SearchAttendanceByUnitNameAsync(string keyword);
    }
}
