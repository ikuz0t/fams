﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IRoleRepository : IGenericRepository<Role>
    {
        public Task<Role> FindRoleByIdAsync(int roleId);
    }
}
