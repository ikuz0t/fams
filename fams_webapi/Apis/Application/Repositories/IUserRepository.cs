﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        Task<User> GetUserByEmailAndPasswordHash(string email, string passwordHash);
        Task<User> GetUserByEmail(string email);

        Task<bool> CheckEmailExited(string email);

        Task<User> FindUserById(int id);


    }
}
