﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IFeedbackRepository : IGenericRepository<Feedback>
    {
        public Task<Feedback> GetFeedbackByIdAsync(int id);
        public Task<IEnumerable<Feedback>> GetNonDeletedFeedbackAsync();
    }
}
