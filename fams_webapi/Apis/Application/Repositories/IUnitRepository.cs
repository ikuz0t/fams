﻿using Domain.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;





namespace Application.Repositories
{
    public interface IUnitRepository : IGenericRepository<Unit>
    {
        public Task<Unit> GetSyllabusID(int SyllabusID);

        public Task<Unit> GetUnitID(int UnitID);
        public Task<Unit> GetIdUnitByAsync(int id);

    }
}
