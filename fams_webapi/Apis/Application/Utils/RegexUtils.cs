﻿namespace Application.Utils
{
    public class RegexUtils
    {
        private const string _mailRegex = "^[\\w.+\\-]+@gmail\\.com$";

        public static string GetMailRegex() { return _mailRegex; }
    }
}
