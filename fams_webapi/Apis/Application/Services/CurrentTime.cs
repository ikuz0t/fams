﻿using Application.Interfaces;

namespace Application.Services
{
    public class CurrentTime : ICurrentTime
    {
        public DateTime GetCurrentTime() => DateTime.UtcNow;

        public long GetCurrentUnixTime(DateTime currentTime)
        {
            var date = currentTime;
            var dt2 = new DateTimeOffset(date);
            long a = dt2.ToUnixTimeMilliseconds();
            return a;
        }
    }
}
