﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AttendanceViewModels;
using Application.ViewModels.ClassViewModels;
using AutoMapper;
using Domain.Entities;
using MailKit.Search;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public AttendanceService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }
        public async Task<IEnumerable<Attendance>> GetAttendanceListAsync()
        {
            var AttendanceList = await _unitOfWork.AttendanceRepository.GetAttendanceListAsync();
            if (AttendanceList.IsNullOrEmpty())
            {
                return Enumerable.Empty<Attendance>();
            }
            return AttendanceList;
        }

        public async Task CreateAttendanceAsync(AttendanceCreateDTO attendanceCreateDTO)
        {
            var newAttendance = new Attendance
            {
                Date = _currentTime.GetCurrentTime(),
                UserID = attendanceCreateDTO.UserID,
                UnitID = attendanceCreateDTO.UnitID,
                ClassID = attendanceCreateDTO.ClassID,
                Status = attendanceCreateDTO.Status,
            };
            await _unitOfWork.AttendanceRepository.AddAsync(newAttendance);
            await _unitOfWork.SaveChangeAsync();
        }
        public async Task<string> UpdateAttendanceAsync(UpdateAttendanceDTO updateAttendanceDTO)
        {
            var currentAttendance = await _unitOfWork.AttendanceRepository.GetAttendanceByIdAsync(updateAttendanceDTO.AttendanceID);
            if (currentAttendance == null)
            {
                return "Data not found!";
            }
            currentAttendance.Status = updateAttendanceDTO.Status;
            _unitOfWork.AttendanceRepository.Update(currentAttendance);
            await _unitOfWork.SaveChangeAsync();
            return "Attendance updated!";
        }

        public async Task<string> DeleteAttendanceAsync(int id)
        {
            var currentAttendance = await _unitOfWork.AttendanceRepository.GetAttendanceByIdAsync(id);
            if (currentAttendance == null)
            {
                return "Data not found!";
            }
            currentAttendance.IsDeleted = true;
            _unitOfWork.AttendanceRepository.Update(currentAttendance);
            await _unitOfWork.SaveChangeAsync();
            return "Attendance is deleted";
        }
        public async Task<List<AttendanceListDTO>> SearchAttendanceByUnitNameAsync(string keyword)
        {
            var attendanceSearch = await _unitOfWork.AttendanceRepository.SearchAttendanceByUnitNameAsync(keyword);

            var attendanceList = attendanceSearch.Select(item => new AttendanceListDTO
            {
                ClassID = item.ClassID,
                UnitID = item.UnitID,
                Status = item.Status,
                Date = item.Date,
                UserID = item.UserID
            }).ToList();

            return attendanceList;
        }
    }
}
