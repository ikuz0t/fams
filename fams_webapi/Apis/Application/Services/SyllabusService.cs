﻿using Application.Commons;
using Application.Interfaces;

using Application.ViewModels.HuanSyllabus;

using Application.ViewModels.SyllabusViewModels;
using AutoMapper;
using Domain.Entities;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using System.Data;
using System.Globalization;
using System.Text;

namespace Application.Services
{
    public class SyllabusService : ISyllabusService

    {
     


        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        public SyllabusService(IUnitOfWork unitOfWork,
            IMapper mapper, ICurrentTime currentTime,
            AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<string> DeleteSyllaaaa(int id)

        {
            var item = await _unitOfWork.SyllabusRepository.GetIdByAsync(id);
            if (item is null)
            {
                throw new Exception("Not Found");
            }
            await _unitOfWork.SyllabusRepository.DeleteSyllaaa(item);
            return "Syllabus deleted successfully.";
        }

        public async Task<string> DeleteSyllabusSoft(int id)
        {
            var aaa = await _unitOfWork.SyllabusRepository.GetIdByAsync(id);
            if (aaa != null)
            {
                aaa.IsDeleted = true;
                _unitOfWork.SyllabusRepository.Update(aaa);
                await _unitOfWork.SaveChangeAsync();
                return "Syllabus deleted successfully.";
            }
            else return "Not Found";
        }
        
        public List<SyllabusListDTOO> GetAllSylla()
        {
            var getList = _mapper.Map<List<SyllabusListDTOO>>
                (_unitOfWork.SyllabusRepository.GetSyllabusList());
            if (getList == null)
            {
                throw new Exception("Empty !");
            }
            return getList;
        }
        public async Task<SyllabusDetailDTO> GetDetailsSyllabus(int syllaDetailID)
        {

            var getListDetail = await _unitOfWork.SyllabusRepository.GetSyllabusDetail(syllaDetailID);
            if (getListDetail == null)
            {
                return null;
            }
            return _mapper.Map<SyllabusDetailDTO>(getListDetail);
        }
        public List<SyllabusListDTOO> GetSyllabusBy(string syllabus)
        {
            var getListBy = _mapper.Map<List<SyllabusListDTOO>>
                (_unitOfWork.SyllabusRepository.GetSyllabusListBy(syllabus));
            if (getListBy == null)
            {
                return null;
            }
            return getListBy;
        }


       


        //public async Task UpdateSyllabus(SyllabusUpdateDTO syllabusObject)

        public async Task UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabusObject)

        {


            var Syllabus = await _unitOfWork.SyllabusRepository.GetSyllabusByID(syllabusID);
            if (Syllabus is null)
            {
                throw new Exception("Input invalid please try again!");
            }
            Syllabus.SyllabusName = syllabusObject.SyllabusName;
            Syllabus.SyllabusStatus = syllabusObject.SyllabusStatus;
            Syllabus.SyllabusCode = syllabusObject.SyllabusCode;
            Syllabus.SyllabusDate = syllabusObject.SyllabusDate;
            Syllabus.SyllabusBy = syllabusObject.SyllabusBy;
            Syllabus.Duration = syllabusObject.Duration;
            Syllabus.Description = syllabusObject.Description;
            Syllabus.SyllabusAssessment = syllabusObject.SyllabusAssessment;
            
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO)
        {
            var existed = await _unitOfWork.SyllabusRepository.CheckCodeExited(createSyllabusDTO.SyllabusCode);
            if (existed) return "Already exists";
            var newSyllabus = new Syllabus
            {
                SyllabusName = createSyllabusDTO.SyllabusName,
                SyllabusCode = createSyllabusDTO.SyllabusCode,
                SyllabusDate = createSyllabusDTO.SyllabusDate,
                SyllabusBy = createSyllabusDTO.SyllabusBy,
                Duration = createSyllabusDTO.Duration,
                Description = createSyllabusDTO.Description,
                SyllabusOutline = createSyllabusDTO.SyllabusOutline,
                SyllabusAudience = createSyllabusDTO.SyllabusAudience,
                SyllabusStatus = createSyllabusDTO.SyllabusStatus,
                SyllabusAssessment = createSyllabusDTO.SyllabusAssessment,

            };
            await _unitOfWork.SyllabusRepository.AddAsync(newSyllabus);
            await _unitOfWork.SaveChangeAsync();
            return "";
        }


        public async Task<List<CreateSyllabusDTO>> ImportSyllabusToObject(IFormFile formFile)
        {
            var syllabusConverting = new List<CreateSyllabusDTO>();
            try
            {
                var dataTable = ReadFile(formFile);
                foreach (var dataRow in dataTable.AsEnumerable())
                {
                    var dateString = dataRow.ItemArray[2]?.ToString();
                    Console.WriteLine($"Original Date String: {dateString}");

                    if (DateTime.TryParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime syllabusDate))
                    {
                        Console.WriteLine($"Parsed Date: {syllabusDate}");
                    }
                    else
                    {
                        Console.WriteLine("Failed to parse date string");
                        // Handle the case where the date string couldn't be parsed
                        // Log an error, throw an exception, or handle it in a way appropriate for your application
                    }
                    var syllabus = new CreateSyllabusDTO
                    {
                        SyllabusName = dataRow.ItemArray[0]?.ToString(),
                        SyllabusCode = dataRow.ItemArray[1]?.ToString(),
                        SyllabusDate = syllabusDate,
                        SyllabusBy = dataRow.ItemArray[3]?.ToString(),
                        Duration = dataRow.ItemArray[4]?.ToString(),
                        Description = dataRow.ItemArray[5]?.ToString(),
                        SyllabusOutline = int.Parse(dataRow.ItemArray[6]?.ToString()),
                        SyllabusAudience = int.Parse(dataRow.ItemArray[7]?.ToString()),
                        SyllabusStatus = dataRow.ItemArray[8]?.ToString(),
                        SyllabusAssessment = dataRow.ItemArray[9]?.ToString()
                    };
                    syllabusConverting.Add(syllabus);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            var newSyllabus = _mapper.Map<List<Syllabus>>(syllabusConverting);
            await _unitOfWork.SyllabusRepository.AddRangeAsync(newSyllabus);
            await _unitOfWork.SaveChangeAsync();
            return syllabusConverting;
        }

        private DataTable ReadFile(IFormFile formFile)
        {
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);
                stream.Position = 0;

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var dataSetConfig = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = a => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true,
                        }
                    };
                    return reader.AsDataSet(dataSetConfig).Tables[0];
                }
            }
        }
        //public async Task<string> CreateUnit(CreateUnitDTO createUnitDTO)
        //{
        //    var newUnit = new Unit
        //    {
        //        UnitName = createUnitDTO.UnitName,
        //        Description = createUnitDTO.Description,
        //        NumberLesson = createUnitDTO.NumberLesson,
        //        SyllabusID = createUnitDTO.SyllabusID,
        //    };
        //    await _unitOfWork.SyllabusRepository.AddUnitAsync(newUnit);
        //    await _unitOfWork.SaveChangeAsync();
        //    return SuccessString;
        //}
        //public async Task<string> CreateLesson(SyllabusCreateLessonDTO createLessonDTO)
        //{
        //    var newLesson = new Lesson
        //    {

        //        Duration = createLessonDTO.Duration,
        //        Content = createLessonDTO.Content,
        //        Materials = createLessonDTO.Materials,
        //        TrainingFormat = createLessonDTO.TrainingFormat,
        //        UnitID = createLessonDTO.UnitID,
        //    };
        //    await _unitOfWork.SyllabusRepository.AddLessonAsync(newLesson);
        //    await _unitOfWork.SaveChangeAsync();
        //    return SuccessString;
        //}       
    }
}

    



