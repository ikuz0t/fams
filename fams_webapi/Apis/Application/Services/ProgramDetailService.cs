﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.ProgramDetailViewModel;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class ProgramDetailService : IProgramDetailService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public ProgramDetailService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task CreateProgdAsync(CreateProgramDetailDTO progd)
        {

            var progdlObj = new ProgramDetails 
            { 
                ProgramID = progd.ProgramID,
                SyllabusID = progd.SyllabusID,
                Sequence   = progd.Sequence,
            };
            await _unitOfWork.ProgramDetailRepository.AddAsync(progdlObj);
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task<bool> UpdateProgramDetailAsync(int progd, UpdateProgramDetailDTO updateProgD)
        {
            var existingProgramDetail = await _unitOfWork.ProgramDetailRepository.GetProgDById(progd);
            existingProgramDetail.ProgramID = updateProgD.ProgramID;
            existingProgramDetail.SyllabusID = updateProgD.SyllabusID;
            existingProgramDetail.Sequence = updateProgD.Sequence;          
            await _unitOfWork.SaveChangeAsync();           
            return true;
        }

        public async Task<Pagination<ProgramDetails>> ViewProgramDetailListAsync(int pageIndex, int pageSize) => await _unitOfWork.ProgramDetailRepository.ToPagination(pageIndex, pageSize);


        public async Task DeleteProgramDetailAsync(int programDetailId)
        {
            var programDetail = await _unitOfWork.ProgramDetailRepository.GetProgDById(programDetailId);
            programDetail.IsDeleted = true;
            _unitOfWork.ProgramDetailRepository.Update(programDetail);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
