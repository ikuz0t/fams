﻿using Application;
using Application.Interfaces;
using Application.ViewModels.FeedbackViewModels;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructures.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IUnitOfWork _unitOfWork;
		public FeedbackService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}
		public async Task<Feedback> GetFeedbackByIdAsync(int id) =>
			await _unitOfWork.FeedbackRepository.GetFeedbackByIdAsync(id);
		public async Task<IEnumerable<Feedback>> GetNonDeletedFeedbackAsync()
		{
			var FeedbackList = await _unitOfWork.FeedbackRepository.GetNonDeletedFeedbackAsync();
			if (FeedbackList.IsNullOrEmpty())
			{
				return Enumerable.Empty<Feedback>();
			}
			return FeedbackList;
		}
		public async Task CreateFeedbackAsync(CreateFeedbackDTO createFeedbackDTO)
		{
			var newFeedback = new Feedback
			{
				StudentId = createFeedbackDTO.StudentId,
				TrainerId = createFeedbackDTO.TrainerId,
				FeedbackMessage = createFeedbackDTO.FeedbackMessage,
			};
			await _unitOfWork.FeedbackRepository.AddAsync(newFeedback);
			await _unitOfWork.SaveChangeAsync();
        }
		public async Task<string> UpdateFeedbackAsync(UpdateFeedbackDTO updateFeedbackDTO)
		{
			var currentFeedback = await _unitOfWork.FeedbackRepository.GetFeedbackByIdAsync(updateFeedbackDTO.FeedbackId);
			if (currentFeedback == null)
			{
				return "Data not found!";
			}
			currentFeedback.FeedbackMessage = updateFeedbackDTO.FeedbackMessage;
			_unitOfWork.FeedbackRepository.Update(currentFeedback);
			await _unitOfWork.SaveChangeAsync();
			return "Feedback updated!";
		}
		public async Task<string> DeleteFeedBackAsync(int id)
		{
			var currentFeedback = await _unitOfWork.FeedbackRepository.GetFeedbackByIdAsync(id);
			if (currentFeedback == null)
			{
				return "Data not found!";
			}
			currentFeedback.IsDeleted = true;
			_unitOfWork.FeedbackRepository.Update(currentFeedback);
			await _unitOfWork.SaveChangeAsync();
			return "Feedback is deleted";
		}
	}
}
