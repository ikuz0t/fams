
using Application.ViewModels.SyllabusViewModels;
using Application.ViewModels.HuanSyllabus;
using Microsoft.AspNetCore.Http;



namespace Application.Interfaces
{
    public interface ISyllabusService
    {
        List<SyllabusListDTOO> GetAllSylla();
        Task<SyllabusDetailDTO> GetDetailsSyllabus(int syllaDetailID);
        List<SyllabusListDTOO> GetSyllabusBy(string syllabus);       
        Task<string> DeleteSyllaaaa(int id);
        Task<string> DeleteSyllabusSoft(int id);
        
        //Task UpdateSyllabus(SyllabusUpdateDTO syllabus);
        Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO);        
        Task UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabus);
        public Task<List<CreateSyllabusDTO>> ImportSyllabusToObject(IFormFile formFile);

    }
}
