﻿using Application.Commons;
using Application.ViewModels.LessonViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface ILessonService
    {
        public Task<Pagination<Lesson>> ViewLessonListAsync(int pageIndex, int pageSize);
        public Task CreateLessonAsync(CreateLessonDTO lesson);
        public Task DeleteLessonByIdAsync(int contentId);
        Task<bool> UpdateLessonAsync(int contentId, UpdateLessonDTO updateLesson);

        
    }
}
