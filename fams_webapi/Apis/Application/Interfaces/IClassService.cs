﻿using Application.ViewModels.ClassViewModels;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces
{
    public interface IClassService
	{
		public Task<List<ClassDTO>> ViewClassListAsync();

		public Task<ClassDetailsDTO> ViewClassDetailAsync(int classId);

		public Task<List<ClassDTO>> SearchClassAsync(string keyword);

		public Task CreateClassAsync(ClassCreateDTO classNameObject);

		public Task UpdateClassAsync(ClassUpdateDTO classNameObject);

		public Task DeleteClassAsync(int classId);

		public Task AddStudentsIntoClassAsync(ClassStudentsCreateDTO dto);
        public Task<List<ClassExcelDTO>> ImportClassToObject(IFormFile formFile);
    }
}
