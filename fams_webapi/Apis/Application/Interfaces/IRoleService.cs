﻿using Domain.Entities;

namespace Application.Interfaces
{
    public interface IRoleService
    {

        public Task<Role> GetRoleAsync(int roleId);

        public Task<List<Role>> GetAllRoleAsync();

    }
}
