
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

﻿using Application.ViewModels.HuanSyllabus;
using Domain.Entities;


namespace Application.Interfaces
{
    public interface IUnitService
    {

        Task<string> DeleteUnitSoft(int id);

        public Task CreateUnit(CreateUnitDTO createUnitObject);
        public Task UpdateUnit(int UnitID, CreateUnitDTO updateUnitObject);
        
        public Task CreateLesson(Lesson lesson);
        Task DeleteUnit(int unitID);

    }
}
