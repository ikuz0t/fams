﻿using Application.ViewModels.TokenModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IUserService
    {

        public Task<List<UserViewModel>> GetUsers();
        public Task<UserViewModel> GetUserByIdAsync(int id);
        public User GetUserById(int id);
        public Task<string> ForgotPassword(string email, string password, bool confirm, long unixTime);
        public Task<List<UserViewModel>> GetUserByFullName(string FullName);
        public Task<List<UserViewModel>> GetUserByEmail(string Email);
        public Task Register(UserRegisterModel userObject);
        public Task<AuthenticateResponse> Login(UserLoginDTO userObject);
        public Task ChangePassword(UserChangePasswordModel userObject);
        public Task ChangeInfo(int id, UserChangeInfoModel userObject);
        public Task SendEmail(UserForgotPasswordModel model);

    }
}
