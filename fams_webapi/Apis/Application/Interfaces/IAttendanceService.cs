﻿using Application.ViewModels.AttendanceViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IAttendanceService
    {
        public Task CreateAttendanceAsync(AttendanceCreateDTO attendanceCreateDTO);
        public Task<string> UpdateAttendanceAsync(UpdateAttendanceDTO attendanceUpdateDTO);
        public Task<string> DeleteAttendanceAsync(int id);
        public Task<IEnumerable<Attendance>> GetAttendanceListAsync();
        public Task<List<AttendanceListDTO>> SearchAttendanceByUnitNameAsync(string keyword);
    }
}
