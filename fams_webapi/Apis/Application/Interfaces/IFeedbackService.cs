﻿using Application.ViewModels.FeedbackViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IFeedbackService
    {
        public Task<Feedback> GetFeedbackByIdAsync(int id);
        public Task<IEnumerable<Feedback>> GetNonDeletedFeedbackAsync();
        public Task CreateFeedbackAsync(CreateFeedbackDTO createFeedbackDTO);
        public Task<string> UpdateFeedbackAsync(UpdateFeedbackDTO updateFeedbackDTO);
        public Task<string> DeleteFeedBackAsync(int id);
    }
}
