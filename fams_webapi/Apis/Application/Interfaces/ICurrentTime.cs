﻿namespace Application.Interfaces
{
    public interface ICurrentTime
    {
        public DateTime GetCurrentTime();
        public long GetCurrentUnixTime(DateTime currentTime);
    }
}
