﻿using Application.Commons;
using Application.ViewModels.ProgramDetailViewModel;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IProgramDetailService
    {
        public Task<Pagination<ProgramDetails>> ViewProgramDetailListAsync(int pageIndex, int pageSize);

        public Task CreateProgdAsync(CreateProgramDetailDTO progd);

        Task<bool> UpdateProgramDetailAsync(int progd, UpdateProgramDetailDTO updateProgD);


        public Task DeleteProgramDetailAsync(int programDetailId);

    }
}
