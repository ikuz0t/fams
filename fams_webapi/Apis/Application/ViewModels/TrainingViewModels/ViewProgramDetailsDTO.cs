﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TrainingViewModels
{
    public class ViewProgramDetailsDTO
    {
        public string TrainingName { get; set; }
        public DateTime TrainingDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public List<Syllabus> Syllabus { get; set; }
    }
}
