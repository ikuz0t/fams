﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SyllabusViewModels
{
    public class UnitListDTO
    {
        public int UnitID { get; set; }       
        public string UnitName { get; set; }     
        public string Description { get; set; }
        public int NumberLesson { get; set; }       
        public int SyllabusID { get; set; }
        public Boolean IsDelete { get; set; }
        public List<LessonListDTO> Lessons { get; set; }
    }
}
