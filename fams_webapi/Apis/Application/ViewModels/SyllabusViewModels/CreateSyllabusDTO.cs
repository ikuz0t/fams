﻿using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.HuanSyllabus
{
    public class CreateSyllabusDTO
    {
        public string SyllabusName { get; set; }
        public string SyllabusCode { get; set; }
        public DateTime SyllabusDate { get; set; }
        public string SyllabusBy { get; set; }
        public string Duration { get; set; }
        public string Description { get; set; }
        public int? SyllabusOutline { get; set; }
        public int? SyllabusAudience { get; set; }
        public string SyllabusStatus { get; set; }
        public string SyllabusAssessment { get; set; }


    }
}
