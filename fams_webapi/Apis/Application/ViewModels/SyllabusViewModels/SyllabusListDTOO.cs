﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SyllabusViewModels
{
    public class SyllabusListDTOO
    {
        public int SyllabusID { get; set; }
        public string SyllabusName { get; set; }
        public string SyllabusCode { get; set; }
        public DateTime SyllabusDate { get; set; }
        public string SyllabusBy { get; set; }
        public string Duration { get; set; }
    }
}
