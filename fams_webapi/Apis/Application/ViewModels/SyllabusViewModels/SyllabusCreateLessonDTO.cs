﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.HuanSyllabus
{
    public class SyllabusCreateLessonDTO
    {
        //public int ContentID { get; set; }
        public string Duration { get; set; }
        public string Content { get; set; }
        public string Materials { get; set; }
        public string TrainingFormat { get; set; }
        public int UnitID { get; set; }
    }
}
