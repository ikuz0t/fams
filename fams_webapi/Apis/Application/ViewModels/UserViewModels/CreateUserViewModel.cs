﻿using Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.UserViewModels
{
    public class CreateUserViewModel
    {
        public int UserID { get; set; }

        public int RoleID { get; set; }

        public string Email { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;

        public string FullName { get; set; } = string.Empty;

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public string Gender { get; set; } = string.Empty;

        public int Level { get; set; }

        public string Status { get; set; } = string.Empty;

        public Role? Role { get; set; }
    }
}
