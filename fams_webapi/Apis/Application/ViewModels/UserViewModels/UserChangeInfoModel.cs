﻿using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.UserViewModels
{
    public class UserChangeInfoModel
    {
        public string FullName { get; set; } = string.Empty;

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public string Gender { get; set; } = string.Empty;

        public int RoleId { get; set; }
    }
}
