﻿namespace Application.ViewModels.FeedbackViewModels
{
    public class UpdateFeedbackDTO
    {
        public int FeedbackId { get; set; }
        public string FeedbackMessage { get; set; }
    }
}
