﻿namespace Application.ViewModels.FeedbackViewModels
{
    public class CreateFeedbackDTO
    {
        public int StudentId { get; set; }
        public int TrainerId { get; set; }
        public string FeedbackMessage { get; set; }
    }
}
