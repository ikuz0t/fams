﻿using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassCreateDTO
    {
        public string ClassName { get; set; }

        public string CreatedBy { get; set; }

        public string Duration { get; set; }

        public string Location { get; set; }

		public string FSU { get; set; }

        public int ProgramID { get; set; }

		public string ClassTime { get; set; }

		public string LocationDetail { get; set; }

		public string Admin { get; set; }

		public string Trainer { get; set; }

	}
}
