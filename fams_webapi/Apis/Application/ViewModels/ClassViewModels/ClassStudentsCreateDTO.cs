﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
	public class ClassStudentsCreateDTO
	{
		public int ClassId { get; set; }

		public List<int> StudentId { get; set; }
	}
}
