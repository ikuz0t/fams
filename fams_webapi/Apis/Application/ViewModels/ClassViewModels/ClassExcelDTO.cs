﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassExcelDTO
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string ClassCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreateBy { get; set; }
        public string Duration { get; set; }
        //public string Attendee { get; set; }
        public string Location { get; set; }
        public string FSU { get; set; }
        public string? ClassTime { get; set; }

        [StringLength(100)]
        public string? LocationDetail { get; set; }

        [StringLength(100)]
        public string? Admin { get; set; }

        [StringLength(100)]
        public string? Trainer { get; set; }
        public int ProgramID { get; set; }


    }
}
