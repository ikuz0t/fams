﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
	public class ClassDTO
	{
		public int ClassId { get; set; }
		public string ClassName { get; set; }
		public string ClassCode { get; set; }
		public DateTime CreatedOn { get; set; }
		public string CreateBy { get; set; }
		public string Duration { get; set; }
		//public string Attendee { get; set; }
		public string Location { get; set; }
		public string FSU { get; set; }
		public bool? IsDelete { get; set; }
	}
}
