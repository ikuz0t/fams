﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AttendanceViewModels
{
    public class AttendanceCreateDTO
    {

        public DateTime Date { get; set; }

        public int UserID { get; set; }

        public int ClassID { get; set; }

        public int UnitID { get; set; }

        public string Status { get; set; }
    }
}
