﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AttendanceViewModels
{
    public class UpdateAttendanceDTO
    {
        public int AttendanceID { get; set; }
        public string Status { get; set; }
    }
}
