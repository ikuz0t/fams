﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        

        public IUserRepository UserRepository { get; }

        public ISyllabusRepository SyllabusRepository { get; }
        public IUnitRepository UnitRepository { get; }

        public IRoleRepository RoleRepository { get; }

        public ITokenRepository TokenRepository { get; }

        public ITrainingRepository TrainingRepository { get; }
        public ILessonRepository LessonRepository { get; }
        public IProgramDetailRepository ProgramDetailRepository { get; }

        public IClassRepository ClassRepository { get; }
       
        public IFeedbackRepository FeedbackRepository { get; }

        public IAttendanceRepository AttendanceRepository { get; }

  

        public Task<int> SaveChangeAsync();
    }
}
