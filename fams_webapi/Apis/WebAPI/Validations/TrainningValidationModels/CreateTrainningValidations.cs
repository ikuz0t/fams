using Application.ViewModels.LessonViewModels;
using Application.ViewModels.TrainingViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateTrainningValidations : AbstractValidator<CreateTrainingDTO>
    {
        public CreateTrainningValidations()
        {
            RuleFor(training => training.ProgramID).NotNull().NotEmpty().WithMessage("ProgramID is something wrong or invalid");
            RuleFor(training => training.TrainingName).NotNull().Length(0, 100).WithMessage("TrainingName must be longer than 1 and less than 100");
            RuleFor(training => training.CreatedBy).NotNull().Length(0, 100).WithMessage("CreatedBy must be longer than 1 and less than 100");

        }
    }
}
