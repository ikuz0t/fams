using Application.ViewModels.LessonViewModels;
using Application.ViewModels.ProgramDetailViewModel;
using FluentValidation;

namespace WebAPI.Validations
{
    public class UpdateProgramValidations : AbstractValidator<UpdateProgramDetailDTO>
    {
        public UpdateProgramValidations()
        {
            RuleFor(program => program.SyllabusID).NotNull().NotEmpty().WithMessage("SyllabusID is invalid");
            RuleFor(program => program.ProgramID).NotNull().NotEmpty().WithMessage("ProgramID is invalid");
        }
    }
}
