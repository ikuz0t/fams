using Application.ViewModels.LessonViewModels;
using Application.ViewModels.ProgramDetailViewModel;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateProgramValidations : AbstractValidator<CreateProgramDetailDTO>
    {
        public CreateProgramValidations()
        {
            RuleFor(program => program.ProgramDetailId).NotNull().NotEmpty().WithMessage("ProgramDetailId is invalid");
            RuleFor(program => program.SyllabusID).NotNull().NotEmpty().WithMessage("SyllabusID is invalid");
            RuleFor(program => program.ProgramID).NotNull().NotEmpty().WithMessage("ProgramID is invalid");

        }
    }
}
