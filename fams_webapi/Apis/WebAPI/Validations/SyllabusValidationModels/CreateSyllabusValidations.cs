using Application.ViewModels.HuanSyllabus;
using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateSyllabusValidations : AbstractValidator<CreateSyllabusDTO>
    {
        public CreateSyllabusValidations()
        {
            RuleFor(syllabus => syllabus.SyllabusCode).NotNull().NotEmpty().WithMessage("SyllabusCode must be Enter");
            RuleFor(syllabus => syllabus.SyllabusAudience).NotNull().NotEmpty().WithMessage("SyllabusAudience must be Enter");
            RuleFor(syllabus => syllabus.SyllabusOutline).NotNull().NotEmpty().WithMessage("SyllabusOutline must be Enter");

        }
    }
}
