using Application.ViewModels.LessonViewModels;
using Application.ViewModels.SyllabusViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class UpdateSyllabusValidations : AbstractValidator<SyllabusUpdateDTO>
    {
        public UpdateSyllabusValidations()
        {
            RuleFor(syllabus => syllabus.SyllabusCode).NotNull().WithMessage("SyllabusCode has not be null");
            RuleFor(syllabus => syllabus.SyllabusBy).NotNull().WithMessage("SyllabusBy has not be null");
            RuleFor(syllabus => syllabus.SyllabusAssessment).NotNull().WithMessage("SyllabusAssessment has not be null");
            RuleFor(syllabus => syllabus.Duration).NotNull().WithMessage("Duration has not be null");

        }
    }
}
