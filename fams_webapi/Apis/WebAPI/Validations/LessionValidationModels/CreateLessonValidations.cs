using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateLessonValidations : AbstractValidator<CreateLessonDTO>
    {
        public CreateLessonValidations()
        {
            RuleFor(lesson => lesson.ContentID).Empty().NotEmpty().WithMessage("ContentID is Not valid or Something wrong");
            RuleFor(lesson => lesson.Duration).NotNull();
            RuleFor(lesson => lesson.UnitID).NotNull().NotEmpty().WithMessage("UnitID is something wrong");
        }
    }
}
