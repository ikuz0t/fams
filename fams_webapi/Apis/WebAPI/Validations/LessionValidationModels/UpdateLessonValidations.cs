using Application.ViewModels.LessonViewModels;
using Domain.Entities;
using FluentValidation;

namespace WebAPI.Validations
{
    public class UpdateLessonValidations : AbstractValidator<Lesson>
    {
        public UpdateLessonValidations()
        {
            RuleFor(lesson => lesson.ContentID).NotNull().NotEmpty().WithMessage("ContentID is wrong or invalid");
            RuleFor(lesson => lesson.UnitID).NotNull().NotEmpty().WithMessage("UnitID must not be null");
        }
    }
}
