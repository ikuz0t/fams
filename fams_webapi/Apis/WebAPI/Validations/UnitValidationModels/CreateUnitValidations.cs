using Application.ViewModels.HuanSyllabus;
using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateUnitValidations : AbstractValidator<CreateUnitDTO>
    {
        public CreateUnitValidations()
        {
            RuleFor(unit => unit.SyllabusID).NotNull().NotEmpty().WithMessage("SyllabusID is invalid");
            RuleFor(unit => unit.UnitName).NotNull().NotEmpty().Length(0, 100).WithMessage("UnitName must be longer than 1 and less than 100");
            RuleFor(unit => unit.NumberLesson).NotNull().NotEmpty().WithMessage("NumberLesson is invalid");

        }
    }
}
