﻿using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateLessonViewModelValidation : AbstractValidator<CreateLessonDTO>
    {
        public CreateLessonViewModelValidation()
       {
            RuleFor(x => x.Content).NotEmpty().MaximumLength(100);
        }
    }
}
