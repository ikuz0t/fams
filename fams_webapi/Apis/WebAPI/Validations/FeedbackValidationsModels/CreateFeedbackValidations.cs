using Application.ViewModels.FeedbackViewModels;
using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateFeedbackValidations : AbstractValidator<CreateFeedbackDTO>
    {
        public CreateFeedbackValidations()
        {
            RuleFor(feedback => feedback.StudentId).Empty().NotEmpty().WithMessage("StudentID is Not valid or Something wrong");
            RuleFor(feedback => feedback.TrainerId).Empty().NotEmpty().WithMessage("TrainerID is Not valid or Something wrong");

        }
    }
}
