using Application.ViewModels.FeedbackViewModels;
using Application.ViewModels.LessonViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class UpdateFeedbackValidations : AbstractValidator<UpdateFeedbackDTO>
    {
        public UpdateFeedbackValidations()
        {
            RuleFor(feedback => feedback.FeedbackId).Empty().NotEmpty().WithMessage("FeedbackId is Not valid or Something wrong");
            
            
        }
    }
}
