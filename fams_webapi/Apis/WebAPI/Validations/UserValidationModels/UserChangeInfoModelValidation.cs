﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations.UserValidationModels
{
    public class UserChangeInfoModelValidation : AbstractValidator<UserChangeInfoModel>
    {
        public UserChangeInfoModelValidation() 
        {
            RuleFor(user => user.DateOfBirth).LessThan(DateTime.Today.AddDays(1)).WithMessage("Must be a valid date.");
            
            Unless(user => user.Gender.Length == 0, () => {
                RuleFor(user => user.Gender).Length(1, 9).WithMessage("Gender should be less than 10 characters.");
            });

            Unless(user => user.FullName.Length == 0, () => {
                RuleFor(user => user.FullName).Length(4, 100).WithMessage("Name should be longer than 4 characters");
            });
               
        }

    }
}
