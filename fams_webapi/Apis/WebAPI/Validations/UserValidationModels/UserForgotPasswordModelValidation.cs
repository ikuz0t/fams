﻿using Application.Utils;
using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations.UserValidationModels
{
    public class UserForgotPasswordModelValidation : AbstractValidator<UserForgotPasswordModel>
    {

        public UserForgotPasswordModelValidation() 
        {
            RuleFor(user => user.Email).NotNull().NotEmpty().Matches(RegexUtils.GetMailRegex()).WithMessage("Not a valid Gmail.");
            RuleFor(user => user.Password).NotEmpty().NotNull().Length(5, 50).WithMessage("Should be between 5 to 50 characters.");
        }
    }
}
