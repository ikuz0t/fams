 using Application.ViewModels.ClassViewModels;
using Domain.Entities;
using FluentValidation;

namespace WebAPI.Validations.ClassValidationModels
{
    public class CreateClassValidations : AbstractValidator<ClassCreateDTO>
    {
        public CreateClassValidations()
        {
            RuleFor(create => create.ClassName).NotEmpty().NotNull().Length(4, 100).WithMessage("Name should be longer than 4 characters"); 
            RuleFor(create => create.Location).NotEmpty().WithMessage("please Input the location");
            RuleFor(create => create.ProgramID).NotEmpty().WithMessage("please Input programID");
            RuleFor(create => create.FSU).NotEmpty().WithMessage("please Input FSU");
        }

    }
}

