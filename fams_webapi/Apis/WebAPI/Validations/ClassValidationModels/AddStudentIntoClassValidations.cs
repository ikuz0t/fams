using Application.ViewModels.ClassViewModels;
using Domain.Entities;
using FluentValidation;

namespace WebAPI.Validations.ClassValidationModels
{
    public class AddStudentIntoClassValidations : AbstractValidator<ClassStudentsCreateDTO>
    {
        public AddStudentIntoClassValidations()
        {
            RuleFor(addstudent => addstudent.ClassId).NotNull().NotEmpty().WithMessage("The ClassID not match or invalid");
            RuleFor(addstudent => addstudent.StudentId).NotNull().NotEmpty().WithMessage("The StudentID not match or invalid");

        }

    }
}