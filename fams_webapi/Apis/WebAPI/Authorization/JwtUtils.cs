﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace WebAPI.Authorization
{
    public interface IJwtUtils
    {
        public string? GenerateJwtToken(User user);
        public int? ValidateJwtToken(string? token);
    }

    public class JwtUtils : IJwtUtils
    {
        private readonly AppConfiguration _appConfiguration;
        private readonly ICurrentTime _currentTime;

        public JwtUtils(AppConfiguration appConfiguration, ICurrentTime currentTime)
        {
            _appConfiguration = appConfiguration;
            _currentTime = currentTime;
        }

        public string? GenerateJwtToken(User user)
        {
            return GenerateJsonWebTokenString.GenerateJsonWebToken(user, _appConfiguration.JWTSecretKey, _currentTime.GetCurrentTime());
        }

        public int? ValidateJwtToken(string? token)
        {
            if (token == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var secret = Encoding.ASCII.GetBytes(_appConfiguration.JWTSecretKey);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secret),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

                return userId;
            }
            catch
            {
                return null;
            }
        }
    }
}
