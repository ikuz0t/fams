using Application.Interfaces;
using Application.ViewModels.FeedbackViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("[controller]")]
    public class FeedbackController : BaseController
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        [HttpGet("GetNonDeletedFeedbackAsync")]
        public async Task<IEnumerable<Feedback>> GetNonDeletedFeedbacks() =>
            await _feedbackService.GetNonDeletedFeedbackAsync();

        [HttpGet("GetFeedbackByIdAsync")]
        public async Task<Feedback> GetFeedbackById(int id) =>
            await _feedbackService.GetFeedbackByIdAsync(id);

        [HttpPost("CreateFeedbackAsync")]
        public async Task CreateFeedbackAsync(CreateFeedbackDTO dto) =>
            await _feedbackService.CreateFeedbackAsync(dto);

        [HttpPut("UpdateFeedbackAsync")]
        public async Task<string> UpdateFeedbackAsync(UpdateFeedbackDTO updateFeedbackDTO) =>
            await _feedbackService.UpdateFeedbackAsync(updateFeedbackDTO);

        [HttpDelete("DeleteFeedbackAsync")]
        public async Task<string> DeleteFeedbackAsync(int id) =>
            await _feedbackService.DeleteFeedBackAsync(id);
	}
}