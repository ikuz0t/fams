﻿using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Application.ViewModels.AttendanceViewModels;
using Application.ViewModels.ClassViewModels;
using Application.ViewModels.FeedbackViewModels;
using Domain.Entities;
using Infrastructures.Repositories;
using Infrastructures.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("[controller]")]
    public class AttendanceController : BaseController
    {
        private readonly IAttendanceService _attendanceService;

        public AttendanceController(IAttendanceService attendanceService)
        {
            _attendanceService = attendanceService;
        }
        [HttpPost("GetAttendanceList")]
        public async Task<IEnumerable<Attendance>> GetAttendanceList() =>
        await _attendanceService.GetAttendanceListAsync();
        [HttpGet("SearchAttendanceByUnitName")]
        public async Task<IActionResult> SearchAttendanceByUnitName(string keyword)
        {
            List<AttendanceListDTO> results = await _attendanceService.SearchAttendanceByUnitNameAsync(keyword);

            if (results.Count == 0)
            {
                return NotFound("Không tìm thấy kết quả nào.");
            }

            return Ok(results);
        }

        [HttpPost("CreateAttendance")]
        public async Task CreateAttendanceAsync(AttendanceCreateDTO attendanceCreateDTO) =>
           await _attendanceService.CreateAttendanceAsync(attendanceCreateDTO);
        [HttpPut("UpdateAttendance")]
        public async Task<string> UpdateAttendanceAsync(UpdateAttendanceDTO updateAttendanceDTO) =>
            await _attendanceService.UpdateAttendanceAsync(updateAttendanceDTO);
        [HttpPut("DeleteAttendance")]
        public async Task<string> DeleteAttendanceAsync(int id) =>
            await _attendanceService.DeleteAttendanceAsync(id);
    }
}
