﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.LessonViewModels;
using Application.ViewModels.TrainingViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class LessonController : BaseController
    {
        private readonly ILessonService _lessonService;

        public LessonController(ILessonService lessonService)
        {
            _lessonService = lessonService;
        }
        [HttpGet]
        public async Task<Pagination<Lesson>> ViewLessonListAsync(int pageIndex = 1, int pageSize = 10)
        {
            return await _lessonService.ViewLessonListAsync(pageIndex, pageSize);
        }

        [HttpPost]
        public async Task CreateLesson(CreateLessonDTO lesson)
        {
             await _lessonService.CreateLessonAsync(lesson);
        }

        [HttpPost]
        public async Task DeleteLessonByIdAsync(int contentId) => await _lessonService.DeleteLessonByIdAsync(contentId);
        [HttpPut]
        public async Task UpdateLessonAsync(int contentId, UpdateLessonDTO lesson)
        {
            await _lessonService.UpdateLessonAsync(contentId, lesson);
        }
    }
}
