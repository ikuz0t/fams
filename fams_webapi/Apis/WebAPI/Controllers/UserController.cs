using Application.Interfaces;
using Application.ViewModels.TokenModels;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Authorization;

namespace WebAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;

        }

        [AllowAnonymous]
        [HttpGet("GetUsers")]
        public async Task<List<UserViewModel>> GetAllUsers() => await _userService.GetUsers();

        [AllowAnonymous]
        [HttpGet("GetUserByIdAsync")]
        public async Task<UserViewModel> GetUserById(int id) => await _userService.GetUserByIdAsync(id);

        [AllowAnonymous]
        [HttpGet("GetUserByFullName")]
        public async Task<List<UserViewModel>> GetUserByFullName(string FullName) => await _userService.GetUserByFullName(FullName);

        [AllowAnonymous]
        [HttpGet("GetUserByEmail")]
        public async Task<List<UserViewModel>> GetUserByEmail(string Email) => await _userService.GetUserByEmail(Email);

        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task Register(UserRegisterModel loginObject)
        {
            await _userService.Register(loginObject);
            if (!ModelState.IsValid)
            {
                await Task.FromResult(StatusCode(StatusCodes.Status400BadRequest, ModelState));
            }
            await Task.FromResult(StatusCode(StatusCodes.Status200OK));
        }


        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<AuthenticateResponse> Login(UserLoginDTO loginObject) => await _userService.Login(loginObject);

        
        [HttpPut("ChangePassword")]
        public async Task ChangePassword(UserChangePasswordModel userObject) => await _userService.ChangePassword(userObject);

        [AllowAnonymous]
        [HttpPut("ChangeInfo")]
        public async Task ChangeInfo(int id, UserChangeInfoModel userObject) => await _userService.ChangeInfo(id, userObject);

        [AllowAnonymous]
        [HttpPost("SendEmail")]
        public async Task SendEmail(UserForgotPasswordModel model) => await _userService.SendEmail(model);


        [AllowAnonymous]
        [HttpGet("ForgotPassword")]
        public async Task<string> ForgotPassword(string email, string password, bool confirm, long unixTime)
        {
            var response = await _userService.ForgotPassword(email, password, confirm, unixTime);
            return response;
        }
    }
}