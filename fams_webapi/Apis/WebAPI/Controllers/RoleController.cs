﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        public async Task<List<Role>> GetAllRoles() => await _roleService.GetAllRoleAsync();

        [HttpPost]
        public async Task<Role> GetRoleById(int id) => await _roleService.GetRoleAsync(id);
    }
}
