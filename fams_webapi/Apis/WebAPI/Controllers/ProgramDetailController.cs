﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.LessonViewModels;
using Application.ViewModels.ProgramDetailViewModel;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class ProgramDetailController : BaseController
    {
        private readonly IProgramDetailService _programDetailService;

        public ProgramDetailController(IProgramDetailService programDetailService)
        {
            _programDetailService = programDetailService;
        }
        [HttpGet]
        public async Task<Pagination<ProgramDetails>> ViewProgramDetailListAsync(int pageIndex = 1, int pageSize = 10)
        {
            return await _programDetailService.ViewProgramDetailListAsync(pageIndex, pageSize);
        }
        [HttpPost]
        public async Task CreateProgd(CreateProgramDetailDTO progd) => await _programDetailService.CreateProgdAsync(progd);
        [HttpPut]
        public async Task UpdateProgramDetailAsync(int programDetailId, UpdateProgramDetailDTO updateProgramDetail) => await _programDetailService.UpdateProgramDetailAsync(programDetailId, updateProgramDetail);

        [HttpPost]
        public async Task DeleteProgramDetailAsync(int programDetailId) => await _programDetailService.DeleteProgramDetailAsync(programDetailId);

    }
}
