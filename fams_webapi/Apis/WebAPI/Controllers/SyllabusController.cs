﻿using Application.Interfaces;


using Application.ViewModels.HuanSyllabus;

using Application.ViewModels.SyllabusViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;

namespace WebAPI.Controllers
{
    public class SyllabusController : BaseController
    {



        private readonly ISyllabusService _syllabusService;

        public SyllabusController(ISyllabusService syllabusService)
        {
            _syllabusService = syllabusService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult GetSyllabus()
        {
            var syllabusDTOList = _syllabusService.GetAllSylla();
            if (syllabusDTOList == null)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(syllabusDTOList);
        }
            [HttpGet("{syllabusSearch}")]
            [ProducesResponseType(200)]        
            [ProducesResponseType(404)]
            public IActionResult GetSyllabusBy(
                [Required(ErrorMessage = "SyllaDetailID is required")]            
                string syllabusSearch)
            {
            // Kiểm tra và loại bỏ khoảng trắng đầu và cuối chuỗi
            syllabusSearch = syllabusSearch?.Trim();

            if (string.IsNullOrWhiteSpace(syllabusSearch) || !Regex.IsMatch(syllabusSearch, "^[a-zA-Z0-9]+$"))
            {
                ModelState.AddModelError("SyllabusSearch", "Invalid SyllabusSearch. It must not be null, must not contain special characters, and must contain only letters and numbers.");
                return BadRequest(ModelState);
            }

            var syllabusDTOListBy = _syllabusService.GetSyllabusBy(syllabusSearch);  
                if(syllabusDTOListBy == null || syllabusDTOListBy.Count == 0)
                {
                    return NotFound("Syllabus Not Found");
                }
                return Ok(syllabusDTOListBy);
            }
        [HttpGet("{syllaDetailID}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetSyllabusDetail(string syllaDetailID)
        {           
            syllaDetailID = syllaDetailID?.Trim();
            if (string.IsNullOrEmpty(syllaDetailID) || !int.TryParse(syllaDetailID, out int parsedValue))
            {
                ModelState.AddModelError("SyllaDetailID", "SyllaDetailID must be a valid integer");
                return BadRequest(ModelState);
            }

            int intValue = parsedValue;

            if (intValue <= 0)
            {
                ModelState.AddModelError("SyllaDetailID", "SyllaDetailID must be a positive integer");
                return BadRequest(ModelState);
            }
            var ae = await  _syllabusService.GetDetailsSyllabus(intValue);
                if (ae == null)
                {
                    return NotFound("Syllabus Not Found !");
                }               
            return Ok(ae);                     
        }
       
        [HttpDelete("{deleteReal}")]      
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteSyllabus(string deleteReal)
        {
            deleteReal = deleteReal?.Trim();
            if (string.IsNullOrEmpty(deleteReal) || !int.TryParse(deleteReal, out int parsedValue))
            {
                ModelState.AddModelError("deleteSoft", "deleteSoft must be a valid integer");
                return BadRequest(ModelState);
            }
            int intValue = parsedValue;
            if (intValue <= 0)
            {
                ModelState.AddModelError("deleteSoft", "deleteSoft must be a positive integer");
                return BadRequest(ModelState);
            }
            try
            {

                 var aa = await  _syllabusService.DeleteSyllaaaa(intValue);
                return Ok(aa);
               

                await _syllabusService.DeleteSyllaaaa(intValue);
                return NoContent();

            } catch (Exception ex)
            {
                return BadRequest(new {error = ex.Message});
            }

           
        }
        [HttpPost("{deleteSoft}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteSyllabusSoft(string deleteSoft)
        {
            deleteSoft = deleteSoft?.Trim();

            if (string.IsNullOrEmpty(deleteSoft) || !int.TryParse(deleteSoft, out int parsedValue))
            {
                ModelState.AddModelError("deleteSoft", "deleteSoft must be a valid integer");
                return BadRequest(ModelState);
            }
            int intValue = parsedValue;

            if (intValue <= 0)
            {
                ModelState.AddModelError("deleteSoft", "deleteSoft must be a positive integer");
                return BadRequest(ModelState);
            }
            try
            {
                var aa = await _syllabusService.DeleteSyllabusSoft(intValue);
                return Ok(aa);

            }
            catch (Exception ex)
            {
                return BadRequest(new { error = ex.Message });
            }

        }       

        [HttpPost]
        public async Task<string> UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabusObject)
        {
            await _syllabusService.UpdateSyllabus(syllabusID, syllabusObject);
            return "Success";
        }

        [HttpPost]
        public async Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO) => await _syllabusService.CreateSyllabus(createSyllabusDTO);

        [HttpPost]
        public async Task<IActionResult> ImportSyllabusToObject(List<IFormFile> formFiles)
        {
            foreach (var file in formFiles)
            {
                await _syllabusService.ImportSyllabusToObject(file);
            }
            return Ok("Import successfully");
        }


    }
}
