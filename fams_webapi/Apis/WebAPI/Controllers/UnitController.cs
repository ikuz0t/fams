
using Application.Interfaces;
using Application.ViewModels.HuanSyllabus;

using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class UnitController : BaseController
    {
        private readonly IUnitService _unitService;
        public UnitController(IUnitService unitService)
        {
            _unitService = unitService;
        }

        [HttpPost("{deleteUnitSoft}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteUnitSoft(int deleteUnitSoft)
        {
            try
            {
                var aa = await _unitService.DeleteUnitSoft(deleteUnitSoft);
                return Ok(aa);

            }
            catch (Exception ex)
            {
                return BadRequest(new { error = ex.Message });
            }

        }




        [HttpPost]
        public async Task<string> CreateUnit(CreateUnitDTO createUnitDTO)
        {
            await _unitService.CreateUnit(createUnitDTO);
            return "Success";
        }
        [HttpPut]
        public async Task<string> UpdateUnit(int UnitID, CreateUnitDTO updateUnitDTO)
        {
            await _unitService.UpdateUnit(UnitID, updateUnitDTO);
            return "Success";
        }
        [HttpPut]
        public async Task<string> DeleteUnit(int UnitID)
        {
            await _unitService.DeleteUnit(UnitID);
            return "Success";
        }



    }
}
