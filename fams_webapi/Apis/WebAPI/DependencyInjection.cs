﻿using Application;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using FluentValidation;
using FluentValidation.AspNetCore;
using Infrastructures;
using Infrastructures.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http.Features;
using System.Diagnostics;
using System.Reflection;
using WebAPI.Authorization;
using WebAPI.Middlewares;
using WebAPI.Services;

using Microsoft.AspNetCore.Authentication.Google;

using WebAPI.Validations;
using WebAPI.Validations.UserValidationModels;


namespace WebAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHealthChecks();
            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddSingleton<PerformanceMiddleware>();
            services.AddSingleton<Stopwatch>();

  
            services.AddValidatorsFromAssemblyContaining<UserRegisterModelValidation>(); // any validator will works
            services.AddFluentValidationAutoValidation(); // the same old MVC pipeline behavior
            services.AddFluentValidationClientsideAdapters(); // for client side
            

            services.AddScoped<IClaimsService, ClaimsService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>(); 
            services.AddScoped<IUnitService, UnitService>();
            services.AddScoped<ISyllabusService, SyllabusService>();
            services.AddScoped<IUnitRepository, UnitRepository>();

            services.AddScoped<IEmailService, EmailService>();
			services.AddScoped<IJwtUtils, JwtUtils>();
            

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = GoogleDefaults.AuthenticationScheme;
            })
            .AddGoogle(options =>
            {
                options.ClientId = "1042164448753-tqk73vh5t407fcq72ecareqoau5mc1sg.apps.googleusercontent.com";
                options.ClientSecret = "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";
                options.CallbackPath = "/accounts.google.com/signin/oauth"; // Đảm bảo đúng URI chuyển hướng
            });

            
            services.AddHttpContextAccessor();
            services.AddFluentValidationAutoValidation();
            services.AddFluentValidationClientsideAdapters();
            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = long.MaxValue;
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartHeadersLengthLimit = int.MaxValue;
            });
            return services;
        }
    }
}
