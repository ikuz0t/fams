import { parseJwt } from "./Services/util.service.js";
var navWidth;
var closeNavWidth;
var mainNavMenu;
var loggedUser;
$(document).ready(function () {
  let token = sessionStorage.getItem("loggedUser");
  let memberImage = sessionStorage.getItem("userImg");
  if (token) {
    loggedUser = parseJwt(token);
    console.log(loggedUser);
    var fullName = loggedUser.Name;
    var memberId = loggedUser.Id;
    let defaultImage = "./img/Default_pfp.svg";
    memberImage = memberImage === "" ? defaultImage : memberImage;
    $("#fullName").html(fullName);
    $("#image").attr("src", memberImage);
    $("#member-id").html(memberId);
  }
  mainNavMenu = $("#main-navigation-menu");
  navWidth = mainNavMenu.outerWidth(true);
  $("#main-navigation-menu").css("width", navWidth);
  closeNavWidth = $("#btn-nav-toggle").outerWidth(true) + navWidth - mainNavMenu.width();
  $("#btn-nav-toggle-open-icon").css("opacity", "0.5");
  $("#btn-nav-toggle-open-icon").css("transform", "rotate(-45deg)");
  $("#btn-nav-toggle").click(function () {
    $(this).prop("disabled", true);
    if ($(".nav-collapsed").length === 0) {
      collapseNavbar();
    } else {
      expandNavbar();
    }
    setTimeout(() => {
      $(this).prop("disabled", false);
    }, 150);
  });
  $(".btn-nav").click(function () {
    $(this).prop("disabled", true);
    if ($(".nav-collapsed").length !== 0) {
      expandNavbar();
    }
    setTimeout(() => {
      $(this).prop("disabled", false);
    }, 150);
    $(this).children(".nav-dropdown").toggleClass("nav-dropdown-open");
  });
  $("#logoutButton").click(function () {
    // Clear the session storage
    sessionStorage.removeItem("loggedUser");
    // Redirect to the login page or perform any other desired action
    window.location.href = "login.html";
  });
  $(".collapse").on("show.bs.collapse", function () {
    $(".collapse.show").collapse("hide");
  });
  $(document).on("ajaxStart",function(){
    showLoadingBackdrop();
  });
  $(document).on("ajaxStop",function(){
    hideLoadingBackdrop();
  });
});
function expandNavbar() {
  $(".nav-collapsed").removeClass("nav-collapsed");
  $(".nav-text").removeClass("hide");
  $(".nav-dropdown").removeClass("hide");
  $("#main-navigation-menu").animate({ width: navWidth }, 150, function () {});
  $("#btn-nav-toggle-open-icon").animate(
    {
      opacity: 0.5,
      angle: -45,
    },
    {
      duration: 75,
      step: function (now, fx) {
        if (fx.prop === "angle") {
          $(this).css("transform", "rotate(" + now + "deg)");
        } else {
          $(this).css("opacity", now);
        }
      },
      complete: function () {
        $("#btn-nav-toggle-open-icon").addClass("hide");
        $("#btn-nav-toggle-close-icon").removeClass("hide");
        $("#btn-nav-toggle-close-icon").animate(
          {
            opacity: 1,
            angle: 0,
          },
          {
            duration: 75,
            step: function (now, fx) {
              if (fx.prop === "angle") {
                $(this).css("transform", "rotate(" + now + "deg)");
              } else {
                $(this).css("opacity", now);
              }
            },
          }
        );
      },
    }
  );
}
function collapseNavbar() {
  $("#main-navigation-menu").addClass("nav-collapsed");
  $(".collapse.show").removeClass("show");
  $(".nav-dropdown-open").removeClass("nav-dropdown-open");
  $("#main-navigation-menu").animate(
    { width: closeNavWidth },
    150,
    function () {
      $(".nav-text").addClass("hide");
      $(".nav-dropdown").addClass("hide");
    }
  );
  $("#btn-nav-toggle-close-icon").animate(
    {
      opacity: 0.5,
      angle: 45,
    },
    {
      duration: 75,
      step: function (now, fx) {
        if (fx.prop === "angle") {
          $(this).css("transform", "rotate(" + now + "deg)");
        } else {
          $(this).css("opacity", now);
        }
      },
      complete: function () {
        $("#btn-nav-toggle-close-icon").addClass("hide");
        $("#btn-nav-toggle-open-icon").removeClass("hide");
        $("#btn-nav-toggle-open-icon").animate(
          {
            opacity: 1,
            angle: 0,
          },
          {
            duration: 75,
            step: function (now, fx) {
              if (fx.prop === "angle") {
                $(this).css("transform", "rotate(" + now + "deg)");
              } else {
                $(this).css("opacity", now);
              }
            },
          }
        );
      },
    }
  );
}
function showLoadingBackdrop() {
  $('#loadingBackdrop').addClass('loading-active');
}
function hideLoadingBackdrop() {
  $('#loadingBackdrop').removeClass('loading-active');
}