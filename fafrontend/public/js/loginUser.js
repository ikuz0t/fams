import { googleLogin, loginUser, register } from "./Services/user.service.js";
import { parseJwt } from "./Services/util.service.js";
$(document).ready(function () {
  $("#loginForm").submit(function (event) {
    event.preventDefault(); // prevent the browser's default action on form submit

    var email = $("#userNameInput").val();
    var password = $("#passwordInput").val();

    var data = {
      email: email,
      password: password,
    };

    if (data) {
      loginUser(data)
        .then((response) => {
          console.log(response);
          sessionStorage.setItem("loggedUser", response.token);
          sessionStorage.setItem("userImg", response.imageUrl);
          window.location.href = "index.html";
        })
        .catch((error) => {
          console.error("Error during login:", error);
          Swal.fire({
            icon: "error",
            title: "Login Failed",
            text: "Invalid email or password.",
            showCancelButton: false,
            confirmButtonText: "OK",
            iconHtml: '<span class="custom-x-icon"></span>',
            customClass: {
              popup: "custom-swal-popup",
              title: "custom-swal-title",
              confirmButton: "custom-swal-button",
              icon: "custom-swal-icon", // Custom CSS class for the "X" icon
            },
            target: "body",
            allowOutsideClick: false,
            allowEscapeKey: false,
          });
        });
    }
  });
});
$(document).ready(function () {
  $("#userRegister").click(function (e) {
    e.preventDefault();
    const userName = $("#username").val();
    const email = $("#email").val();
    const password = $("#password").val();
    const phone = $("#phone").val();
    const dob = $("#dob").val();
    var gender = $("input[name='gender']:checked").val() === "true";

    if (userName && email && password && phone && dob && gender !== undefined) {
      const registerUser = {
        name: userName,
        email: email,
        phone: phone,
        dob: dob,
        password: password,
        gender: gender,
      };
      register(registerUser);
    }
  });
});

function handleCallbackRes(res) {
  //Fix email
  const profile = parseJwt(res.credential);
  const userData = {
    email: profile.email,
    name: profile.name,
    imageUrl: profile.picture,
  };
  googleLogin(userData)
    .then((response) => {
      console.log(response);
      sessionStorage.setItem("loggedUser", response.token);
      sessionStorage.setItem("userImg", response.imageUrl);
      window.location.href = "index.html";
    })
    .catch((ex) => {
      console.log(ex);
    });
}

function initializeGoogleSignIn() {
  if (window.google && window.google.accounts && window.google.accounts.id) {
    window.google.accounts.id.initialize({
      client_id:
        "1065578323385-6ukalkhhqs2il0drjb46dbknj3tfos1q.apps.googleusercontent.com",
      callback: handleCallbackRes,
    });

    window.google.accounts.id.renderButton(
      document.getElementById("signInDiv"),
      { theme: "outline", size: "large" }
    );
  } else {
    setTimeout(initializeGoogleSignIn, 100);
  }
}
document.addEventListener("DOMContentLoaded", (event) => {
  initializeGoogleSignIn();
});
